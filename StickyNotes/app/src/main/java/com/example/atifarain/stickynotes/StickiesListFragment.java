package com.example.atifarain.stickynotes;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class StickiesListFragment extends BaseFragment{

    ListView lvStickies;
    TextView tvlvSticky;

    CustomListAdapter stickyAdapter;

    public static final String basePath = Environment.getExternalStorageDirectory().toString()+"/StickyNotes/";
    ArrayList<StickyModel> listDataItems = new ArrayList<StickyModel>();
    int pos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.stickies_list,container,false);
        //call for initialization once on every fragment
        setupFragment();
        return mView;
    }

    //1.
    //This will be called every time when my views created by "OnConfigurationChange"
    @Override
    public void initViews() {
        super.initViews();

        getMainActivity().disableHomeButton();
        getMainActivity().showFAB();
        getMainActivity().setActionBarTitle(R.string.app_name);
        lvStickies = (ListView) mView.findViewById(R.id.lvSticky);
        tvlvSticky = (TextView) mView.findViewById(R.id.tvNoSticky);
    }


    //2. option 2 &3 are safe side and called once in life.
    @Override
    public void initObjects() {
        super.initObjects();
        stickyAdapter = new CustomListAdapter(mContext,R.layout.stickies_list_item,listDataItems);
    }

    //3.
    @Override
    public void initFileSystems() {
        super.initFileSystems();
    }

    //4.
    @Override
    public void initListenerOrAdapter() {
        super.initListenerOrAdapter();
        appLaunch();
        lvStickies.setAdapter(stickyAdapter);

        lvStickies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StickyModel stickyModelObject = (StickyModel) lvStickies.getItemAtPosition(position);
                Log.v("ListItem", stickyModelObject.getDateTime().toString());


                NewStickyFragment editStickyFragment = new NewStickyFragment();
                Bundle args = new Bundle();
                args.putSerializable("StickyModel", stickyModelObject);
                editStickyFragment.setArguments(args);
                getBaseActivity().switchScreen(editStickyFragment, R.id.group);
            }
        });

        lvStickies.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Delete Alert");
                alert.setMessage("Do you really want to delete the record?");
                pos = position;
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        StickyModel stickyModelObject = (StickyModel) lvStickies.getItemAtPosition(pos);
                        String fileToDelete = stickyModelObject.getTitle() + ".txt";

                        DirectoryEnum deleteFileResult = FileSystemUtils.deleteFile(basePath, fileToDelete);
                        switch (deleteFileResult) {
                            case SUCCUESSFULLY_DONE: {
                                Snackbar.make(mView, "Deleted Successfully", Snackbar.LENGTH_SHORT).show();
                                listDataItems.remove(pos);
                                stickyAdapter.notifyDataSetChanged();
                                listStatus();
                            }
                            break;
                            case PROCESS_FAILED:
                                break;
                            case EXISTENCE_ISSUE:
                                Snackbar.make(mView, "File Doesn't Exist", Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                        dialog.dismiss();
                    }
                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();

                return true;
            }
        });
    }

    public void appLaunch(){

        if(FileSystemUtils.filesFound(basePath)){
            listDataItems.clear();
            listDataItems.addAll(FileSystemUtils.listFiles(basePath));
            stickyAdapter.notifyDataSetChanged();
            tvlvSticky.setVisibility(View.GONE);
            lvStickies.setVisibility(View.VISIBLE);
        }
        else {
            tvlvSticky.setVisibility(View.VISIBLE);
            lvStickies.setVisibility(View.GONE);
        }
    }

    public void listStatus(){
        if(listDataItems.size()==0)
            tvlvSticky.setVisibility(View.VISIBLE);
    }
}