package com.example.atifarain.stickynotes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomListAdapter extends ArrayAdapter {

    Context mContext;

    public CustomListAdapter(Context context, int resource, ArrayList<StickyModel> objects) {
        super(context, resource, objects);
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderListItem viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.stickies_list_item, null);
            viewHolder = new ViewHolderListItem();

            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.title);
            viewHolder.tvDateTime = (TextView) convertView.findViewById(R.id.dateTime);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderListItem) convertView.getTag();
        }

        StickyModel model = (StickyModel) getItem(position);
        viewHolder.tvTitle.setText(model.getTitle());
        viewHolder.tvDateTime.setText(model.getDateTime());
        return convertView;
    }
}