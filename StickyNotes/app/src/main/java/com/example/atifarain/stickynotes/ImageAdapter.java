package com.example.atifarain.stickynotes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter{

    Context mContext;

    public ImageAdapter(Context c){
        this.mContext = c;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderGalleryItem viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.gallery_items, null);
            viewHolder = new ViewHolderGalleryItem();

            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderGalleryItem) convertView.getTag();
        }

        int resourceID = parent.getResources().getIdentifier("circle_color_"+(position+1), "drawable", parent.getContext().getPackageName());
        viewHolder.imageView.setImageResource(resourceID);
        return convertView;
    }
}