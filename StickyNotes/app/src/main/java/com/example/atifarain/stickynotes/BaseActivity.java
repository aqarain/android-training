package com.example.atifarain.stickynotes;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    public void popAllFragment() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public Fragment getLastFragment() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            return fm.getFragments().get(fm.getBackStackEntryCount());
        }
        return null;
    }

    public void switchScreen(Fragment frag, int containerID) {
        getSupportFragmentManager().beginTransaction()
                .replace(containerID, frag).addToBackStack(null)
                .commit();
    }

    public void switchScreenWithoutStack(Fragment frag, int containerID) {
        getSupportFragmentManager().beginTransaction()
                .replace(containerID, frag)
                .commit();
    }

    public void addFragment(Fragment frag, int containerID) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerID,
                        frag).commit();
    }

}