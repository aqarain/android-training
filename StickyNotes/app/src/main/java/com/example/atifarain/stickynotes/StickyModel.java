package com.example.atifarain.stickynotes;

import java.io.Serializable;

public class StickyModel implements Serializable {

    String dateTime;
    String color;
    String favorite;
    String title;
    String detail;

    public StickyModel() {
    }

    public StickyModel(String dateTime, String color, String title) {
        this.dateTime = dateTime;
        this.color = color;
        this.title = title;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}