package com.example.atifarain.stickynotes;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class FavListFragment extends BaseFragment{

    ListView lvFavourites;
    TextView tvNoFavourites;

    FavListAdapter favAdapter;

    public static final String basePath = Environment.getExternalStorageDirectory().toString()+"/StickyNotes/";
    ArrayList<StickyModel> favListDataItems = new ArrayList<StickyModel>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fav_list_fragment,container,false);
        //call for initialization once on every fragment
        setupFragment();
        return mView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);

        menu.findItem(R.id.favr8).setVisible(false);
        menu.findItem(R.id.save).setVisible(false);
    }

    //1.
    //This will be called every time when my views created by "OnConfigurationChange"
    @Override
    public void initViews() {
        super.initViews();

        getMainActivity().enableHomeButton();
        getMainActivity().showFAB();
        getMainActivity().setActionBarTitle(R.string.fav);
        lvFavourites = (ListView) mView.findViewById(R.id.lvFav);
        tvNoFavourites = (TextView) mView.findViewById(R.id.tvNoFav);
    }


    //2. option 2 &3 are safe side and called once in life.
    @Override
    public void initObjects() {
        super.initObjects();
        favAdapter = new FavListAdapter(mContext,R.layout.fav_list_items, favListDataItems);
    }

    //3.
    @Override
    public void initFileSystems() {
        super.initFileSystems();
    }

    //4.
    @Override
    public void initListenerOrAdapter() {
        super.initListenerOrAdapter();
        appLaunch();
        lvFavourites.setAdapter(favAdapter);

        lvFavourites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StickyModel stickyModelObject = (StickyModel) lvFavourites.getItemAtPosition(position);
                Log.v("ListItem", stickyModelObject.getDateTime().toString());


                NewStickyFragment editStickyFragment = new NewStickyFragment();
                Bundle args = new Bundle();
                args.putSerializable("StickyModel", stickyModelObject);
                editStickyFragment.setArguments(args);
                getBaseActivity().switchScreen(editStickyFragment, R.id.group);
            }
        });
    }


    public void appLaunch(){

        favListDataItems.clear();
        favListDataItems.addAll(FileSystemUtils.listFavFiles(basePath));

        if(favListDataItems.size()!=0){
            favAdapter.notifyDataSetChanged();
            tvNoFavourites.setVisibility(View.GONE);
            lvFavourites.setVisibility(View.VISIBLE);
        }
        else {
            tvNoFavourites.setVisibility(View.VISIBLE);
            lvFavourites.setVisibility(View.GONE);
        }
    }
}