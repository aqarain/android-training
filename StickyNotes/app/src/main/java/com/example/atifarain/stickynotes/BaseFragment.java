package com.example.atifarain.stickynotes;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.concurrent.atomic.AtomicBoolean;

public class BaseFragment extends Fragment {

    protected Context mContext;
    protected View mView;

    protected AtomicBoolean isFragmentLoaded = new AtomicBoolean(false);

    public void initViews() {
        //Injection Views
        mContext = this.getActivity();
    }

    public MainActivity getMainActivity() {
        return (MainActivity) this.getActivity();
    }


    public BaseActivity getBaseActivity() {
        return (BaseActivity) this.getActivity();
    }

    public void initObjects() {
    }

    public void initListenerOrAdapter() {
    }

    public void initFileSystems() {
    }

    //setup will be called by oncreateView
    public void setupFragment() {
        try {
            // 1. inject view with butterknife or manually
            initViews();

            //Is the fragment loading for the first time or not. First time it will get false
            if (isFragmentLoaded.get() == false) {
                //2. Load object once
                initObjects();
                //3. FileSystems calls once
                initFileSystems();
            }

            //4. rebind the views with listeners or adapter again for renewal created views.
            initListenerOrAdapter();
            //mark current fragment as loaded just recreate the views only.
            isFragmentLoaded.set(true);

        } catch (Exception e) {
            //print exception to log
        }
    }

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}