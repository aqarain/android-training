package com.example.atifarain.stickynotes;

import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class NewStickyFragment extends BaseFragment {

    Gallery gallery;
    ImageButton imgBtn;
    EditText title;
    EditText detail;

    ImageAdapter imageAdapter;
    String selectedColor = "color_1";
    StickyModel currentModel = null;
    boolean imagechanged = false;
    boolean colorChanged = false;

    public static final String basePath = Environment.getExternalStorageDirectory().toString()+"/StickyNotes/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.create_sticky_fragment, container, false);

        Bundle bundle = getArguments();
        if(bundle!=null) {
            if (bundle.getSerializable("StickyModel") != null) {
                currentModel = (StickyModel) bundle.getSerializable("StickyModel");
            }
        }
        setupFragment();
        return mView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);

        menu.findItem(R.id.favr8).setVisible(false);
        menu.findItem(R.id.save).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.save){
            hideSoftKeyboard(getMainActivity(),mView);
            saveRecord();
        }

        return super.onOptionsItemSelected(item);
    }

    //1.
    //This will be called every time when my views created by "OnConfigurationChange"
    @Override
    public void initViews() {
        super.initViews();

        getMainActivity().enableHomeButton();
        getMainActivity().hideFAB();
        if(currentModel!=null)
            getMainActivity().setActionBarTitle(R.string.edit_Note);
        else
            getMainActivity().setActionBarTitle(R.string.new_Note);

        gallery = (Gallery) mView.findViewById(R.id.gallery);
        imgBtn = (ImageButton) mView.findViewById(R.id.imageButton);
        title = (EditText) mView.findViewById(R.id.editText2);
        detail = (EditText) mView.findViewById(R.id.editText);
    }

    //2 option 2 &3 are safe side and called once in life.
    @Override
    public void initObjects() {
        super.initObjects();
        imageAdapter = new ImageAdapter(getActivity());

        if (currentModel!=null){
            String bgColor = currentModel.getColor();
            int colorResourceID = getResources().getIdentifier(bgColor, "color", getActivity().getPackageName());
            int colorCode = getResources().getColor(colorResourceID);
            mView.setBackgroundColor(colorCode);

            imgBtn.setVisibility(View.VISIBLE);

            if(currentModel.getFavorite().equals("1")) {
                imagechanged = true;
                imgBtn.setImageResource(R.drawable.favorite_pressed);
            }
            else {
                imagechanged = false;
                imgBtn.setImageResource(R.drawable.favorite_off);
            }


            title.setText(currentModel.getTitle());
            title.setEnabled(false);

            detail.setText(currentModel.getDetail());
        }
    }

    //3
    @Override
    public void initFileSystems() {
        super.initFileSystems();
    }

    //4
    @Override
    public void initListenerOrAdapter() {
        super.initListenerOrAdapter();
        gallery.setAdapter(imageAdapter);
        gallery.setSelection(2);

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedColor = "color_" + (position + 1);
                int colorResourceID = getResources().getIdentifier(selectedColor, "color", getActivity().getPackageName());
                int colorCode = getResources().getColor(colorResourceID);
                mView.setBackgroundColor(colorCode);
                colorChanged = true;
            }
        });

        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!imagechanged) {
                    imgBtn.setImageResource(R.drawable.favorite_pressed);
                    imagechanged = true;
                } else {
                    imgBtn.setImageResource(R.drawable.favorite_off);
                    imagechanged = false;
                }
            }
        });
    }

    public void saveRecord(){
        Log.v("SaveButton", "After clicking Save button");

        if(currentModel!=null){

            String[] editStickyData = new String[5];
            //get system date and time when saving
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            String formattedDate = df.format(c.getTime());
            editStickyData[0] = formattedDate.toString();                         //Date and Time

            if(colorChanged)
                editStickyData[1] = selectedColor;
            else
                editStickyData[1] = currentModel.getColor();                         //Color code

            if(imagechanged)                                                    //favorite/not
                editStickyData[2] = "1";
            else
                editStickyData[2] = "0";

            editStickyData[3] = title.getText().toString();            //title
            editStickyData[4] = detail.getText().toString();           //detail

            //Creating new File
            String fileName = title.getText().toString() + ".txt";
            DirectoryEnum fileUpdateResult = FileSystemUtils.updateFile(basePath, fileName, editStickyData);
            switch (fileUpdateResult) {
                case SUCCUESSFULLY_DONE:
                    Log.v("SaveButton", "File with Content Created Successfully");
                    Snackbar.make(mView, "Saved Successfully", Snackbar.LENGTH_SHORT).show();
                    getBaseActivity().popAllFragment();
                    break;
                case PROCESS_FAILED:
                    Log.v("SaveButton", "Directory Doesn't exist");
                    break;
                case EXISTENCE_ISSUE:
                    Log.v("SaveButton", "File with Content Already Exists");
                    Snackbar.make(mView, "Change Note's Title", Snackbar.LENGTH_SHORT).show();
                    break;
            }
        }
        else{
            String[] newStickyData = new String[5];

            //get system date and time when saving
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            String formattedDate = df.format(c.getTime());

            Log.v("SaveButton", formattedDate);
            newStickyData[0] = formattedDate.toString();                         //Date and Time
            newStickyData[1] = selectedColor;                         //Color code
            newStickyData[2] = "0";                                   //favorite/not
            newStickyData[3] = title.getText().toString();            //title

            newStickyData[4] = detail.getText().toString();           //detail

            colorChanged = false;
            //Creating Directory
            DirectoryEnum creationResult = FileSystemUtils.createDirectory(basePath);
            switch (creationResult) {
                case SUCCUESSFULLY_DONE:
                    Log.v("SaveButton", "Directory Successfully Created");
                    break;
                case PROCESS_FAILED:
                    Log.v("SaveButton", "Failed to create directory");
                    break;
                case EXISTENCE_ISSUE:
                    Log.v("SaveButton", "Directory already exists");
                    break;
            }

            //Creating new File
            String fileName = title.getText().toString() + ".txt";
            DirectoryEnum fileWithContentResult = FileSystemUtils.createFileWithContent(basePath, fileName, newStickyData);
            switch (fileWithContentResult) {
                case SUCCUESSFULLY_DONE:
                    Log.v("SaveButton", "File with Content Created Successfully");
                    Snackbar.make(mView, "Saved Successfully", Snackbar.LENGTH_SHORT).show();
                    getBaseActivity().popAllFragment();
                    break;
                case PROCESS_FAILED:
                    Log.v("SaveButton", "Directory Doesn't exist");
                    break;
                case EXISTENCE_ISSUE:
                    Log.v("SaveButton", "File with Content Already Exists");
                    Snackbar.make(mView, "Change Note's Title", Snackbar.LENGTH_SHORT).show();
                    break;
            }
        }
    }
}