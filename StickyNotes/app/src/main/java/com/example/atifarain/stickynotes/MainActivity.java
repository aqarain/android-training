package com.example.atifarain.stickynotes;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;


public class MainActivity extends BaseActivity {

    Toolbar toolbar;
    FragmentManager manager;
    ImageButton FAB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);               //This tells android that I am not using your action bar rather using my own.
        //getSupportActionBar().setTitle("Hey");      //Set Toolbar title here

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        FAB = (ImageButton) findViewById(R.id.imageButton);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewStickyFragment createStickyFragment = new NewStickyFragment();
                switchScreen(createStickyFragment, R.id.group);
            }
        });

        manager = getFragmentManager();
        stickiesList();
    }

    public void setActionBarTitle(int id){
        getSupportActionBar().setTitle(getString(id));
    }

    public void stickiesList() {
        StickiesListFragment stickiesListFragment = new StickiesListFragment();
        addFragment(stickiesListFragment, R.id.group);
    }

    public void showFAB(){
        FAB.setVisibility(View.VISIBLE);
    }

    public void hideFAB(){
        FAB.setVisibility(View.GONE);
    }

    public void enableHomeButton(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void disableHomeButton(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.getItem(0).setVisible(false);
        menu.findItem(R.id.save).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id==R.id.action_settings){
        }

        if (id ==R.id.favr8){
            FavListFragment favListFragment = new FavListFragment();
            switchScreen(favListFragment, R.id.group);
        }

        if (id == R.id.save){

        }

        return super.onOptionsItemSelected(item);
    }
}