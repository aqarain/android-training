package com.company;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by atif.arain on 8/24/2015.
 */
public class FileSystemUtil {

    public static DirectoryEnum createDirectory(String path, String dirName) {
        String directoryPath = path + dirName;

        File directory = new File(directoryPath);
        if (!directory.exists()) {
            if (directory.mkdir()) {
                return DirectoryEnum.SUCCUESSFULLY_DONE;
            } else {
                return DirectoryEnum.PROCESS_FAILED;
            }
        } else
            return DirectoryEnum.EXISTENCE_ISSUE;
    }

    public static boolean deleteDirectory(File filePath) {
        if (filePath.isDirectory()) {
            File[] children = filePath.listFiles();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDirectory(children[i]);
                if (!success) {
                    return false;
                }
            }
        }
        return filePath.delete();
    }

    public static DirectoryEnum createFile(String path, String dirName, String fileName) {
        File filePath = new File(path + dirName);
        String filePathFull = path + dirName + "\\" + fileName;

        try {
            File file = new File(filePathFull);

            if (!file.exists() && filePath.exists())
                file.createNewFile();
            else if (!filePath.exists())
                return DirectoryEnum.PROCESS_FAILED;
            else
                return DirectoryEnum.EXISTENCE_ISSUE;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return DirectoryEnum.SUCCUESSFULLY_DONE;
    }

    public static DirectoryEnum createFileWithContent(String path, String dirName, String fileName, String[] data) {
        File filePath = new File(path + dirName);
        String filePathFull = path + dirName + "\\" + fileName;

        try {
            File file = new File(filePathFull);

            //if file doesn't exists, then create it
            if (!file.exists() && filePath.exists())
                file.createNewFile();
            else if (!filePath.exists())
                return DirectoryEnum.PROCESS_FAILED;
            else
                return DirectoryEnum.EXISTENCE_ISSUE;

            FileWriter fw = new FileWriter(filePathFull);
            for (String content : data) {
                fw.write(content + "\r\n");
            }
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return DirectoryEnum.SUCCUESSFULLY_DONE;
    }

    public static ArrayList<StickyModel> listFiles(String path, String dirName) {
        ArrayList<StickyModel> listDataItems = new ArrayList<StickyModel>();
        String filePath = path + dirName;
        ArrayList<String> paths;
        try {
            File file = new File(filePath);
            paths = new ArrayList<String>(Arrays.asList(file.list()));
            for (String _path : paths) {
                System.out.println(filePath + File.separator + _path);
                listDataItems.add(readDataFromFile(filePath + File.separator + _path));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listDataItems;
    }

    public static StickyModel readDataFromFile(String path) {

        StickyModel stickyModel = new StickyModel();
        try {
            List<String> fileContent = Files.readAllLines(Paths.get(path));
            if (fileContent != null && fileContent.size() > 0) {
                stickyModel.setDateTime(fileContent.get(0));
                stickyModel.setColor(fileContent.get(1));
                stickyModel.setFavorite(fileContent.get(2));
                stickyModel.setTitle(fileContent.get(3));
                stickyModel.setDetail(fileContent.get(4));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return stickyModel;
    }


    public static DirectoryEnum deleteFile(String path, String dirName, String fileToDelete) {
        String filePath = path + dirName + "\\" + fileToDelete;
        try {
            File file = new File(filePath);
            if (file.exists())
                file.delete();
            else
                return DirectoryEnum.EXISTENCE_ISSUE;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DirectoryEnum.SUCCUESSFULLY_DONE;
    }

    public static DirectoryEnum truncateFile(String path, String dirName, String fileName) {
        String filePath = path + dirName + "\\" + fileName;
        FileWriter fw;

        try {
            fw = new FileWriter(filePath, false);
            fw.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return DirectoryEnum.SUCCUESSFULLY_DONE;
    }


    public static DirectoryEnum updateFile(String path, String dirName, String fileName, String content) {

        String filePath = path + dirName + "\\" + fileName;
        Writer output = null;
        try {
            File file = new File(filePath);
            output = new BufferedWriter(new FileWriter(file, false));
            output.write(content);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return DirectoryEnum.SUCCUESSFULLY_DONE;
    }
}