package com.company;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Main {

    public static void main(String[] args) {

        FileSystemUtil fsu = new FileSystemUtil();
        String basePath = "D:\\Atif\\Training Projects\\";
        String dirName = "Stickies";

        DirectoryEnum creationResult = fsu.createDirectory(basePath, dirName);
        switch (creationResult){
            case SUCCUESSFULLY_DONE:
                System.out.println("Directory Successfully Created");
                break;
            case PROCESS_FAILED:
                System.out.println("Failed to create directory");
                break;
            case EXISTENCE_ISSUE:
                System.out.println("Directory already exists");
                break;
        }



/*        boolean deletionResult = fsu.deleteDirectory(new File(basePath+dirName));
        if(deletionResult)
            System.out.println("Directory Successfully deleted");
        else
            System.out.println("Directory does not exist");




        String fileName = "abc.txt";
        DirectoryEnum fileCreationResult = fsu.createFile(basePath,dirName,fileName);
        switch (fileCreationResult){
            case SUCCUESSFULLY_DONE:
                System.out.println("File Created Successfully");
                break;
            case PROCESS_FAILED:
                System.out.println("Directory Doesn't Exist");
                break;
            case EXISTENCE_ISSUE:
                System.out.println("File Already Exists");
                break;
        }
*/



        String[] newStickyData = new String[5];
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String formattedDate = df.format(c.getTime());

        newStickyData[0] = formattedDate;
        newStickyData[1] = "color-2";
        newStickyData[2] = "0";
        newStickyData[3] = "Test";
        newStickyData[4] = "This is a test file!";

        String contentFileName = "Test.txt";
        DirectoryEnum fileWithContentResult = fsu.createFileWithContent(basePath,dirName,contentFileName,newStickyData);
        switch (fileWithContentResult){
            case SUCCUESSFULLY_DONE:
                System.out.println("File with Content Created Successfully");
                break;
            case PROCESS_FAILED:
                System.out.println("Directory Doesn't exist");
                break;
            case EXISTENCE_ISSUE:
                System.out.println("File with Content Already Exists");
                break;
        }




/*        String fileToDelete = "xyz.txt";
        DirectoryEnum deleteFileResult = fsu.deleteFile(basePath,dirName,fileToDelete);
        switch (deleteFileResult){
            case SUCCUESSFULLY_DONE:
                System.out.println("File Deleted Successfully");
                break;
            case PROCESS_FAILED:
                System.out.println("File Deletion Process Failed");
                break;
            case EXISTENCE_ISSUE:
                System.out.println("File Doesn't Exists");
                break;
        }
*/
        fsu.listFiles(basePath,dirName);

/*
        String fileToTruncate = "xyz.txt";
        DirectoryEnum truncationResult = fsu.truncateFile(basePath,dirName,fileToTruncate);
        switch (truncationResult){
            case SUCCUESSFULLY_DONE:
                System.out.println("File Truncated Successfully");
                break;
            case PROCESS_FAILED:
                System.out.println("File Truncation Process Failed");
                break;
            case EXISTENCE_ISSUE:
                System.out.println("File Doesn't Exists");
                break;
        }



        String newContent = "Thank God, Finally it is done";
        String fileToUpdate = "abc.txt";
        DirectoryEnum updateFileResult = fsu.updateFile(basePath, dirName, fileToUpdate, newContent);
        switch (updateFileResult){
            case SUCCUESSFULLY_DONE:
                System.out.println("File Updated Successfully");
                break;
            case PROCESS_FAILED:
                System.out.println("File Updation Process Failed");
                break;
            case EXISTENCE_ISSUE:
                System.out.println("File Doesn't Exists");
                break;
        }


*/
    }
}